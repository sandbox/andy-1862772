<?php
/**
 * @file
 * Provides a simple unified sign-up form for multiple lists.
 *
 * @todo Add support for interest groups.
 */

/**
 * Implements hook_menu_alter(). Replaces mailchimp/subscribe with our custom
 * page callback that uses the new form.
 */
function mclisthelper_menu_alter(&$items) {
  if (isset($items['mailchimp/subscribe'])) {
    $items['mailchimp/subscribe']['page callback'] = 'mclisthelper_freeform_subscribe_page';
  }
}

/**
 * Implements hook_block_info().
 */
function mclisthelper_block_info() {
  return array(
    'freeform' => array(
      'info' => t("MailChimp Subscription Form: All"),
      'cache' => DRUPAL_CACHE_PER_USER,
    ),
  );
}

/**
 * Implements hook_block_view().
 */
function mclisthelper_block_view($delta) {
  global $user;
  
  switch ($delta) {
  
    case 'freeform':
      
      $lists = mailchimp_lists_get_available_lists($user, MAILCHIMP_LISTTYPE_FREEFORM);
      if ($lists) {
        $block['subject'] = t("Subscribe to our newsletters");
        $block['content'] = drupal_get_form('mclisthelper_user_subscribe_form', $lists, $user);
      }
      return $block;
      break;
  }
}

/**
 * Page callback for the Newsletter Subscription page. Uses the new sign-up
 * form.
 */
function mclisthelper_freeform_subscribe_page() {
  global $user;
  
  $lists = mailchimp_lists_get_available_lists($user, MAILCHIMP_LISTTYPE_FREEFORM);
  
  if (count($lists) == 0) {
    return (t('There are no available newsletter subscriptions.'));
  }
  
  return drupal_get_form('mclisthelper_user_subscribe_form', $lists, $user);
}

/**
 * Return all freeform subscription forms for a given user.
 *
 * @param $lists
 *   An array of mailchimp_list entities.
 *
 * @see mailchimp_lists_auth_newsletter_form()
 */
function mclisthelper_user_subscribe_form($form, &$form_state, $lists, $account) {
  
  // Prune non freeform lists and ensure the remaining lists are keyed by list
  // ID.
  foreach ($lists as $list) {
    if ($list->list_type == MAILCHIMP_LISTTYPE_FREEFORM) {
      $new_lists[$list->id] = $list;
    }
  }
  $lists = $new_lists;
  
  if ($lists) {

    $form['#attributes'] = array(
      'class' => array('mailchimp-lists-user-subscribe-form'),
    );
    $form['account'] = array('#type' => 'value', '#value' => $account);
    $form['title'] = array(
      '#type' => 'item',
      '#markup' => t("Sign up for our mailing lists"),
    );
    
    
    // Add the actual mailing list checkboxes. Use a #after_build to add list
    // descriptions to individual checkboxes.
    $options = array();
    foreach ($lists as $list) {
      $options[$list->id] = $list->label;
    }
    $form['mailchimp_lists'] = array(
      '#type' => 'checkboxes',
      '#options' => $options,
      '#lists' => $lists,
      '#after_build' => array('mclisthelper_user_subscribe_form_after_build'),
    );
    
  
    // Add the merge fields (fields with the same name are collapsed into one
    // another).
    $mergevars = array('#tree' => TRUE);
    foreach ($lists as $list) {
      $subform = array('#tree' => TRUE);
      mailchimp_lists_auth_newsletter_form($subform, $list, $account);
      $mergevars += $subform['mailchimp_' . $list->name]['mergevars'];
    }
    $form['mergevars'] = $mergevars;
    
    
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );
  }

  return $form;
}

/**
 * Submit handler for mclisthelper_user_subscribe_form: adds/updates user
 * subscription to lists as necessary.
 *
 * @see mailchimp_lists_user_subscribe_form_submit()
 */
function mclisthelper_user_subscribe_form_submit($form, &$form_state) {
  
  $account = $form_state['values']['account'];
  $mcapi = mailchimp_get_api_object();
  
  foreach ($form_state['values']['mailchimp_lists'] as $list_id) {
  
    if ($list_id) {
      $list = $form['mailchimp_lists']['#lists'][$list_id];
      if ($list->list_type == 'freeform') {
      
        $mergevars = array();
        foreach ($list->settings['mergefields'] as $field => $mapping) {
          if (!empty($form_state['values']['mergevars'][$field])) {
            $mergevars[$field] = $form_state['values']['mergevars'][$field];
          }
        }
    
        $ret = TRUE;
        $mail = $mergevars['EMAIL'];

        $is_subscribed = mailchimp_is_subscribed($list->mc_list_id, $mail);

        if ($is_subscribed) {
          $ret = mailchimp_update_user($list, $mail, $mergevars, TRUE, $mcapi);
        }
        else {
          $ret = mailchimp_subscribe_user($list, $mail, $mergevars, TRUE, $mcapi);
        }

        if (!$ret) {
          drupal_set_message(t('There was a problem with your newsletter signup: @msg', array('@msg' => $mcapi->errorMessage)));
        }
      }
    }
  }
}

/**
 * #after_build callback for MailChimp list checkboxes. Adds the list
 * description.
 */
function mclisthelper_user_subscribe_form_after_build($element) {
  
  foreach (element_children($element) as $id) {
    if (isset($element['#lists'][$id])) {
      $list = $element['#lists'][$id];
      $element[$id]['#description'] = $list->description;
    }
  }
  return $element;
}
