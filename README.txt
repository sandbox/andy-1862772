MailChimp Lists helper
**********************

This simple sandbox module supplies a block to allow a user to subscribe to any
freeform list using checkboxes. If you use the same merge fields on different
lists, the fields will be displayed only once. It also modifies the sign-up
forms displayed on mailchimp/subscribe to use the new form. It doesn't currently
support interest groups, but that could be added if there's any interest
(haha!).

As well as using the block, you can programatically use the form
'mclisthelper_user_subscribe_form' which takes an array of list entities and
the user account as arguments.

This functionality belongs in the MailChimp project imho, so it will never be
promoted. See #1232744[1] for progress.

Dependencies
************
1. MailChimp Lists (a submodule of the MailChimp project[2])

Links
*****
[1] http://drupal.org/node/1232744
[2] http://drupal.org/project/mailchimp

author: AndyF
http://drupal.org/user/220112

